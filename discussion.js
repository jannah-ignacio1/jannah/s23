//CRUD Operations
//inserting document (CREATE)
/*syntax: db.collectionName.insertOne({
				"fieldA": "valueA",
				"fieldB": "valueB"
			})*/

//insertOne-method, One document

db.users.insertOne({
	"firstName": "jane",
	"lastName": "doe",
	"age": 21,
	"email": "janedoe@mail.com",
	"department": "none"
});

/*syntax: insert many documents
	db.collectioName.insertMany([
		{
			"fieldA": "valueA",
			"fieldB": "valueB"
		},
		{
			"fieldA": "valueA",
			"fieldB": "valueB"
		}
	])
*/

db.users.insertMany([
		{
			"firstName": "stephen",
			"lastName": "hawking",
			"age": 76,
			"email": "stephenhawking@mail.com",
			"department": "none"
		},
		{
			"firstName": "neil",
			"lastName": "armstrong",
			"age": 82,
			"email": "neilarmstrong@mail.com",
			"department": "none"
		}
]);


//Mini Activity
/*new collection: courses
insert the ff values and fields:
name: Javascript 101
price: 5000
description: Introduction to Javascript
isActive: true

name: HTML 101
price: 2000
description: Introduction to HTML
isActive: true

name: CSS 101
price: 2500
description: Introduction to CSS
isActive: false*/

db.courses.insertMany([
		{
		"name": "Javascript 101",
		"price": 5000,
		"description": "Introduction to Javascript",
		"isActive:" true
		},
		{
		"name": "HTML 101",
		"price": 2000,
		"description": "Introduction to HTML",
		"isActive": true
		},
		{
		"name": "CSS 101",
		"price": 2500,
		"description": "Introduction to CSS",
		"isActive": false
		}
	]);

//finding document (READ)
/*syntax: db.collectionName.find() - this will retrieve all our documents
	db.collectionName.find({"criteria": "value"}) - will retrieve all documents that match our criteria
	db.collectionName.findOne({"criteria": "value"}) - will return the first document in out collection that match the criteria
	db.collectionName.findOne({}) - will return the first document in our collection
*/



db.users.find();
db.users.find({"firstName": "jane"});
db.users.findOne({"firstName": "jane"});
db.users.findOne();
db.users.find({"lastName":"armstrong", "age":82})


//updating document (UPDATE)
/*syntax: db.collectionName.updateOne(
			{
				"criteria": "field",
			},
			{
				$set: {
						"fieldToBeUpdated": "updatedValue"
				}
			}
		)
			db.collectionName.updateOne(
			{
				"criteria": "field",
			},
			{
				$set: {
						"fieldToBeUpdated": "updatedValue"
				}
			}
		)
*/


db.users.insertOne({
	"firstName": "Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@mail.com",
	"department": "none"

})


db.users.updateOne(
	{"firstName": "Test"},
	{
		$set: {
			"firstName": "Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@mail.com",
			"department": "Operations",
			"status": "active"
		}
	}
)

//updating MANY or multiple documents
db.users.updateMany(
	{
		"department": "none"
	},
	{
		$set: {
			"department": "HR"
		}
	}
);

//removing a field (REMOVE)
db.users.updateOne(
	{
		"firstName": "Bill"
	},
	{
		$unset: {
			"status": "active"
		}
	}
);

//Mini Activity
/*update HTML 101 Course
make isActive to false*/

db.courses.updateOne(
	{"name": "HTML 101"},
	{
		$set: {
			"isActive": false
		}
	}
)

db.courses.updateMany(
	{}	
		$set: {
			"Enrollees": 10
		}
	
);


//deleting documents (DELETE)
db.users.insertOne({"firstName": "Test"});
/*syntax: db.collectionName.deleteOne({"criteria":"value"})
*/
/*db.collectionName.deleteOne() - will delete the first document*/

db.users.deleteOne({"firstName":"test"});

//Deleteing multiple documents - no criteria will delete everything
db.users.deleteMany({"department": "HR"});
db.courses.deleteMany({});

/*1. Create an activity.js file on where to write and save the solution for the activity.
2. Create a database of hotel with a collection of rooms.
3. Insert a single room (insertOne method) with the following details:

- name - single
- accomodates - 2
- price - 1000
- description - A simple room with all the basic necessities
- rooms_available - 10
- isAvailable - false
4. Insert multiple rooms (insertMany method) with the following details:

- name - double
- accomodates - 3
- price - 2000
- description - A room fit for a small family going on a vacation
- rooms_available - 5
- isAvailable - false

- accomodates - 4
- price - 4000
- description - A room with a queen sized bed perfect for a simple getaway
- rooms_available - 15
- isAvailable - false
5. Use the find method to search for a room with the name double.*/